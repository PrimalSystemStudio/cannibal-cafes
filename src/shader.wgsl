// Shaders are miniprograms sent to gpu to perform operations on data
// The three main types are vertex, fragment and compute

// Vertex shader (point in 2D or 3D space)
// Bundled in groups to form lines or triangles

struct VertexOutput {
    [[builtin(position)]] clip_position: vec4<f32>;
};

[[stage(vertex)]] // Declares this as a valid entry point for a vertex shader
fn vs_main (
    [[builtin(vertex_index)]] in_vertex_index: u32,
) -> VertexOutput {
    var out: VertexOutput;
    let x = f32(1 - i32(in_vertex_index)) * 0.5;
    let y = f32(i32(in_vertex_index & 1u) * 2 - 1) * 0.5;
    out.clip_position = vec4<f32>(x, y, 0.0, 1.0); // saves clip position to "out"
    return out;
}

// Fragment shader

[[stage(fragment)]] // Declares this as a valid entry point for a fragment shader
fn fs_main(in: VertexOutput) -> [[location(0)]] vec4<f32> { // The [[location(0)]] bit tells WGPU to store the vec4 value returned by this function in the first color target.
    return vec4<f32>(0.3, 0.2, 0.1, 1.0); // Sets color of current fragment to brown
}