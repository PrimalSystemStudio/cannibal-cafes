use ggez::*;

// All the data required to represent current game state
struct State {
    is_playing: bool,
    phase: i8,
}

impl State {
    // Create a new game state
    pub fn new(_ctx: &mut Context) -> GameResult<State> {
        let new_game = State {
            is_playing: false,
            phase: 0,
        };
        Ok(new_game)
    }

    // Start game main loop with event::run
    pub fn start() -> ! {
        let c_b = ContextBuilder::new("cannibal-cafes", "PrimalSystemStudio")
            .window_setup(conf::WindowSetup::default().title("Cannibal Cafes"))
            .window_mode(conf::WindowMode::default().dimensions(800.0, 800.0));
        let (mut ctx, event_loop) = c_b.build().unwrap();
        let state = State::new(&mut ctx).unwrap();
        event::run(ctx, event_loop, state);
    }
}

// Context gives GGEZ access to hardware
// GameResult signifies whether there was an error
impl event::EventHandler<GameError> for State {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        Ok(())
    }
    // Draw updates for each game loop
    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        Ok(())
    }
}

pub fn main() -> GameResult {
    State::start();
}