use rand::prelude::*;

/*pub enum Names {
    "The Happyburgh Harvest",
    "Hannibal's Delectebles",
    "Ackille's Meal",
    "Murder King",
    "Papa Hack's",
}*/

pub struct Players {
    num_players: i8, //2..5
}

pub struct Player {
    rep: i8, //2..12
    money: i16,
    stress: i8, //0..6,
    garlic: i8,
    paprika: i8,
    saffron: i8,
    occ_tables: i8, //0..4,
    starting_token: bool,
}

pub struct GameState {
    players_num: i8,
    players: Vec<Player>,
    garlic: i8,
    paprika: i8,
    saffron: i8,
    detective: i8, //0..25
    phase: i8, //1..5
    customers: Vec<CGroup>,
}

pub struct CGroup {
    loc: Option<Player>,
    table: i8, //0..4
    step: i8, //-1..3
    size: i8,
    spice: Spice,
    character: Character,
    
}

pub enum Spice {
    Garlic,
    Paprika,
    Saffron,
}

pub enum Character {
    Normie,
    Rotten,
    Critic,
}

fn main() {
    <GameData as UserInterface>::run(WindowSettings {
        title: String::from("Cannibal Cafes"),
        size: (1280, 1024),
        resizable: true,
        fullscreen: false,
        maximized: false,
    })
    .expect("An error occured while starting the game");
}

// Card images
struct Deck {
    distraught: Image,
    fleshy: Image,
    freaking: Image,
    healthy: Image,
    paranoia: Image,
    rumours: Image,
    scrawny: Image,
    suspicious: Image,
}

impl Deck {
    fn load() -> Task<Deck> {
        (
            Image::load("src/img/c-distraught.png"),
            Image::load("src/img/c-fleshy.png"),
            Image::load("src/img/c-freaking.png"),
            Image::load("src/img/c-healthy.png"),
            Image::load("src/img/c-paranoia.png"),
            Image::load("src/img/c-rumours.png"),
            Image::load("src/img/c-scrawny.png"),
            Image::load("src/img/c-suspicious.png"),
        )
        .join()
        .map(
            |(distraught, fleshy, freaking, healthy, paranoia, rumours, scrawny, suspicious)|
            Deck {
                distraught, fleshy, freaking, healthy, paranoia, rumours, scrawny, suspicious
            })
    }
}

// Start screen buttons
struct Screen1 {
    new_game: button::State,
    load_game: button::State,
}

impl Screen1 {
    fn load() -> Task<Screen1> {
        Task::succeed(|| Screen1 {
            new_game: button::State::new(),
            load_game: button::State::new(),
        })
    }
}

// Game state and assets
struct GameData {
    // Images
    title: Image,
    m_board: Image,
    p_board: Image,
    card_back: Image,
    cards: Deck,
    // Buttons
    save_load: Screen1,
}

// Initialize game assets
impl GameData {
    // Initialize game data
    fn load() -> Task<GameData> {
        (
            Image::load("src/img/title.png"),
            Image::load("src/img/base.png"),
            Image::load("src/img/playerboard.png"),
            Image::load("src/img/card-back.png"),
            Task::stage("Loading cards...", Deck::load()),
            Task::stage("Loading start screen...", Screen1::load())
        )
            .join()
            .map(|(title, m_board, p_board, card_back, cards, save_load)| GameData { title, m_board, p_board, card_back, cards, save_load })
    }
    
}

impl Game for GameData {
    type Input = KeyboardAndMouse;
    type LoadingScreen = ProgressBar; 

    fn load(_window: &Window) -> Task<Self> {
        Task::stage("Loading game data...", GameData::load())
    }

    fn draw(&mut self, frame: &mut Frame, _timer: &Timer) {
        // Clear the current frame
        frame.clear(Color::from_rgb(89,36,13));
    }
}

// User interface for the game
impl UserInterface for GameData {
    type Message = Message;
    type Renderer = Renderer;

    fn react(&mut self, event: Message, _window: &mut Window) {
        match event {
            Message::NewGame => {
                println!("new game button pressed");
            }
            Message::LoadGame => {
                println!("load game button pressed");
            }
        }
    }
    fn layout(&mut self, window: &Window) -> Element<Message> {
        let s1_title = Row::new()
            .width(window.width() as u32)
            .height(159 as u32)
            .align_items(Align::Center)
            .justify_content(Justify::Center)
            .push(Image_ui::new(&self.title));
        let mut s1_new = Row::new()
            .width(window.width() as u32)
            .height(159 as u32)
            .align_items(Align::Center)
            .justify_content(Justify::Center)
            .push(Button::new(&self.save_load.new_game, "New Game").on_press(Message::NewGame));
        let mut s1_load = Row::new()
            .width(window.width() as u32)
            .height(159 as u32)
            .align_items(Align::Center)
            .justify_content(Justify::Center)
            .push(Button::new(&self.save_load.load_game, "Load Game").on_press(Message::LoadGame));

        Column::new()
            .width(window.width() as u32)
            .height(window.height() as u32)
            .align_items(Align::Center)
            .justify_content(Justify::Center)
            .push(s1_title) 
            .push(s1_new)
            .push(s1_load)
            .into()
    }
}

#[derive(Debug, Clone, Copy)]
enum Message {
    NewGame,
    LoadGame,
}
